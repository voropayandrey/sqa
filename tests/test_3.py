
import numpy as np
import sys
import matplotlib
import matplotlib.pyplot as plt
import os
from scipy.io import wavfile

dir_path = os.path.dirname(os.path.realpath(__file__))
parent_dir_path = os.path.abspath(os.path.join(dir_path, os.pardir))
sys.path.insert(0, parent_dir_path)

import sqa.sqa

if __name__ == "__main__":
    offset_seconds = 0
    duration_seconds = 100
    samplerate, data = wavfile.read('../data/3.wav')
    audio_length = len(data)

    resample_to = 2000
    print("Sample rate: ", samplerate)
    print("Duration seconds: ", audio_length / samplerate)
    print("Resample to: ", resample_to)

    offset_samples = offset_seconds * samplerate
    duration_samples = duration_seconds * samplerate
    if duration_samples > len(data[:, 0]):
        duration_samples = len(data[:, 0])

    left = data[offset_samples:duration_samples, 0]
    right = data[offset_samples:duration_samples, 1]

    lower_frequency_hz = 200
    sqa = sqa.sqa.SQA(sample_frequency=samplerate, lower_frequency_hz=lower_frequency_hz, resample_to=resample_to, moving_window_width_percent=800)
    contour_delay_samples = int((resample_to / lower_frequency_hz) * 2)

    sqa.process_all(left, right)

    print("Phonetic interval count: ", len(sqa.phonetic_intervals))
    print("Silence interval count: ", len(sqa.silence_intervals))

    t_list = list(range(0, len(left)))
    t_np = np.array(t_list)
    t_np = t_np * (1.0/samplerate)

    t_resampled_list = list(range(0, len(sqa.input_intensity_history)))
    t_resampled_np = np.array(t_resampled_list)
    t_resampled_np = t_resampled_np * (1.0/resample_to)

    if plt.get_backend() == 'Qt5Agg':
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()

    # Shift contouring_history to left due to the contour_delay_samples
    # contouring_history = sqa.contouring_history[contour_delay_samples:len(sqa.contouring_history)]
    #for i in range(contour_delay_samples):
    #    contouring_history.append(0)

    contouring_history = sqa.contouring_history

    mean_history = sqa.mean_history
    max_history = sqa.max_history
    threshold_history = sqa.threshold_history

    moving_window_gap = len(sqa.input_intensity_history) - len(sqa.mean_history)
    for i in range(moving_window_gap):
        mean_history.append(0)
        max_history.append(0)
        threshold_history.append(0)

    silence_interval_history = np.zeros(len(sqa.input_intensity_history))
    phonetic_interval_history = np.zeros(len(sqa.input_intensity_history))


    for interval in sqa.silence_interval_history:
        start_index = interval[0]
        end_index = interval[1]
        for i in range(start_index, end_index):
            silence_interval_history[i] = sqa.total_max_amplitude

    for interval in sqa.phonetic_interval_history:
        start_index = interval[0]
        end_index = interval[1]
        for i in range(start_index, end_index):
            phonetic_interval_history[i] = sqa.total_max_amplitude


    fig, axs = plt.subplots(nrows=2, ncols=1)#, sharex='all')
    axs[0].plot(t_np, left, '#0000FF', label='left channel', alpha=0.5)
    axs[0].plot(t_np, right, '#FF0000', label='right channel', alpha=0.5)
    axs[0].set_xlabel('Time, s')
    axs[0].set_ylabel('Intensity')
    axs[0].grid(True)
    axs[0].legend(loc='best')
    axs[1].plot(t_resampled_np, sqa.input_intensity_history, '#FF0000', label='input_intensity_history', linewidth='1', alpha=0.5)
    #axs[1].plot(t_resampled_np, sqa.intensity_resampled_history, '#00FF00', label='intensity_resampled_history', linewidth='1', alpha=0.75)
    axs[1].plot(t_resampled_np, contouring_history, '#0000FF', label='contouring_history', linewidth='1', alpha=0.25)
    axs[1].plot(t_resampled_np, mean_history, '#000000', label='mean_history', linewidth='1', alpha=1)
    axs[1].plot(t_resampled_np, max_history, '#FF0000', label='max_history', linewidth='1', alpha=1)
    axs[1].plot(t_resampled_np, threshold_history, '#FFF000', label='threshold_history', linewidth='1', alpha=1)
    axs[1].fill(t_resampled_np, silence_interval_history, '#00FF00', label='silence_interval_history', linewidth='1', alpha=0.5)
    axs[1].fill(t_resampled_np, phonetic_interval_history, '#FF0000', label='phonetic_interval_history', linewidth='1', alpha=0.5)
    axs[1].set_xlabel('Time, s')
    axs[1].set_ylabel('Intensity')
    axs[1].grid(True)
    axs[1].legend(loc='best')

    silence_intervals_t = list()
    sum = 0
    for interval in sqa.silence_intervals:
        silence_intervals_t.append(sum)
        sum += interval / 1000

    phonetic_intervals_t = list()
    sum = 0
    for interval in sqa.phonetic_intervals:
        phonetic_intervals_t.append(sum)
        sum += interval / 1000

    fig2, axs2 = plt.subplots(nrows=2, ncols=1)
    axs2[0].set_xlabel('Time, s')
    axs2[0].set_ylabel('Interval, ms')
    axs2[0].plot(silence_intervals_t, sqa.silence_intervals, '#0000FF', label='silence_intervals', linewidth='1')
    axs2[1].set_xlabel('Time, s')
    axs2[1].set_ylabel('Interval, ms')
    axs2[1].plot(phonetic_intervals_t, sqa.phonetic_intervals, '#0000FF', label='phonetic_intervals', linewidth='1')


    fig3, axs3 = plt.subplots(nrows=2, ncols=1)
    axs3[0].hist(sqa.silence_intervals)
    axs3[1].hist(sqa.phonetic_intervals)

    if plt.get_backend() == 'Qt5Agg':
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
    plt.show()