#!/usr/bin/env python

# Author: Voropay Andrey
# SQA - Speech Quality Analysis
# This is only a command line interface to the SQA library

import sys
import os
import argparse
from scipy.io import wavfile
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import sqa.sqa
import time

parser = argparse.ArgumentParser(description="SQA(Speech Quality Analysis) command line interface to the library")
parser.add_argument("INPUT_FILE_PATH", type=str, help="Input audio data file")
parser.add_argument("--verbose", default=False, dest="IS_VERBOSE", action="store_true", help="Print debug information.")
parser.add_argument("--plot", default=False, dest="DEBUG_PLOT", action="store_true", help="Plot data.")
parser.add_argument("--from", dest="FROM_SECONDS", default=0, type=int, help="Read file from sample.")
parser.add_argument("--to", dest="TO_SECONDS", default=0, type=int, help="Read file to seconds.")
parser.add_argument("--threshold", dest="DETECTOR_THRESHOLD", default=3, type=int, help="Threshold level in percents.")
parser.add_argument("--resample", dest="RESAMPLE_HZ", default=16000, type=int, help="Resample input data to desired frequency.")
parser.add_argument("--lower_frequency_limit", dest="LOWER_FREQUENCY_LIMIT_HZ", default=200, type=int, help="Minimal signal frequency to be processed.")
parser.add_argument("--averaging_window_percents", dest="AVERAGING_WINDOW_PERCENTS", default=2, type=int, help="Minimal signal frequency to be processed.")
parser.add_argument("--moving_window_percents", dest="MOVING_WINDOW_PERCENTS", default=400, type=int, help="Moving window width used for max and mean values.")
args = parser.parse_args()
audioFilePath = args.INPUT_FILE_PATH
isVerbose = args.IS_VERBOSE

def v(*args):
    if isVerbose:
        print(*args)

def plot(left, rigth, sqa, resample_to):
    t_list = list(range(0, len(left)))
    t_np = np.array(t_list)
    t_np = t_np * (1.0 / samplerate)

    t_resampled_list = list(range(0, len(sqa.input_intensity_history)))
    t_resampled_np = np.array(t_resampled_list)
    t_resampled_np = t_resampled_np * (1.0 / resample_to)

    if plt.get_backend() == 'Qt5Agg':
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()

    # Shift contouring_history to left due to the contour_delay_samples
    # contouring_history = sqa.contouring_history[contour_delay_samples:len(sqa.contouring_history)]
    # for i in range(contour_delay_samples):
    #    contouring_history.append(0)

    contouring_history = sqa.contouring_history

    mean_history = sqa.mean_history
    max_history = sqa.max_history
    threshold_history = sqa.threshold_history

    moving_window_gap = len(sqa.input_intensity_history) - len(sqa.mean_history)
    for i in range(moving_window_gap):
        mean_history.append(0)
        max_history.append(0)
        threshold_history.append(0)

    silence_interval_history = np.zeros(len(sqa.input_intensity_history))
    phonetic_interval_history = np.zeros(len(sqa.input_intensity_history))

    for interval in sqa.silence_interval_history:
        start_index = interval[0]
        end_index = interval[1]
        for i in range(start_index, end_index):
            silence_interval_history[i] = sqa.total_max_amplitude

    for interval in sqa.phonetic_interval_history:
        start_index = interval[0]
        end_index = interval[1]
        for i in range(start_index, end_index):
            phonetic_interval_history[i] = sqa.total_max_amplitude

    fig, axs = plt.subplots(nrows=2, ncols=1)  # , sharex='all')
    axs[0].plot(t_np, left, '#0000FF', label='left channel', alpha=0.5)
    axs[0].plot(t_np, right, '#FF0000', label='right channel', alpha=0.5)
    axs[0].set_xlabel('Time, s')
    axs[0].set_ylabel('Intensity')
    axs[0].grid(True)
    axs[0].legend(loc='best')
    axs[1].plot(t_resampled_np, sqa.input_intensity_history, '#FF0000', label='input_intensity_history', linewidth='1', alpha=0.5)
    axs[1].plot(t_resampled_np, contouring_history, '#0000FF', label='contouring_history', linewidth='1', alpha=0.25)
    axs[1].plot(t_resampled_np, mean_history, '#000000', label='mean_history', linewidth='1', alpha=1)
    axs[1].plot(t_resampled_np, max_history, '#FF0000', label='max_history', linewidth='1', alpha=1)
    axs[1].plot(t_resampled_np, threshold_history, '#FFF000', label='threshold_history', linewidth='1', alpha=1)
    axs[1].fill(t_resampled_np, silence_interval_history, '#00FF00', label='silence_interval_history', linewidth='1', alpha=0.5)
    axs[1].fill(t_resampled_np, phonetic_interval_history, '#FF0000', label='phonetic_interval_history', linewidth='1', alpha=0.5)
    axs[1].set_xlabel('Time, s')
    axs[1].set_ylabel('Intensity')
    axs[1].grid(True)
    axs[1].legend(loc='best')

    silence_intervals_t = list()
    sum = 0
    for interval in sqa.silence_intervals:
        silence_intervals_t.append(sum)
        sum += interval / 1000

    phonetic_intervals_t = list()
    sum = 0
    for interval in sqa.phonetic_intervals:
        phonetic_intervals_t.append(sum)
        sum += interval / 1000

    fig2, axs2 = plt.subplots(nrows=2, ncols=1)
    axs2[0].set_xlabel('Time, s')
    axs2[0].set_ylabel('Interval, ms')
    axs2[0].plot(silence_intervals_t, sqa.silence_intervals, '#0000FF', label='silence_intervals', linewidth='1')
    axs2[1].set_xlabel('Time, s')
    axs2[1].set_ylabel('Interval, ms')
    axs2[1].plot(phonetic_intervals_t, sqa.phonetic_intervals, '#0000FF', label='phonetic_intervals', linewidth='1')

    fig3, axs3 = plt.subplots(nrows=2, ncols=1)
    axs3[0].hist(sqa.silence_intervals)
    axs3[1].hist(sqa.phonetic_intervals)

    if plt.get_backend() == 'Qt5Agg':
        figManager = plt.get_current_fig_manager()
        figManager.window.showMaximized()
    plt.show()

if os.path.isfile(audioFilePath):
    v("Python version: ", sys.version)
    if args.DEBUG_PLOT:
        v("Matplotlib backend: " + plt.get_backend())
        matplotlib.use('Qt5Agg')
        v("Matplotlib backend switched to: " + plt.get_backend())

    samplerate, data = wavfile.read(audioFilePath)
    audio_length_samples = len(data)
    print("Sample rate: ", samplerate)
    audio_file_duration_seconds = audio_length_samples / samplerate
    print("File duration seconds: ", audio_file_duration_seconds)

    resample_to = args.RESAMPLE_HZ

    sqa = sqa.sqa.SQA(sample_frequency=samplerate,
                      resample_to=resample_to,
                      threshold_percent=args.DETECTOR_THRESHOLD,
                      lower_frequency_hz=args.LOWER_FREQUENCY_LIMIT_HZ,
                      average_window_percent=args.AVERAGING_WINDOW_PERCENTS,
                      moving_window_width_percent=args.MOVING_WINDOW_PERCENTS)

    from_sample = args.FROM_SECONDS * samplerate
    to_sample = args.TO_SECONDS * samplerate
    if to_sample == 0 or to_sample > audio_length_samples:
        to_sample = audio_length_samples

    left = data[from_sample:to_sample, 0]
    right = data[from_sample:to_sample, 1]
    print("Processing data...")

    processed_samples = 0
    processed_percents = 0
    data_to_process_length_samples = len(left)
    samples_per_percent = data_to_process_length_samples / 100
    print("Processed samples: 0 %")
    processing_start_time_ns = time.time_ns()
    for index in range(data_to_process_length_samples):
        sqa.process_sample(left[index], right[index])
        if processed_samples > samples_per_percent - 1:
            processed_percents += 1
            processed_samples = 0
            sys.stdout.write("\033[F")  # back to previous line
            sys.stdout.write("\033[K")  # clear line
            print("Processed samples:", processed_percents, "%")
        processed_samples += 1
    processing_end_time_ns = time.time_ns()
    processing_time_seconds = (processing_end_time_ns - processing_start_time_ns) / 1000000000
    print("Processing done! Time seconds: ", processing_time_seconds)
    si_q = np.median(sqa.silence_intervals) / len(sqa.silence_intervals)
    pi_q = np.median(sqa.phonetic_intervals) / len(sqa.phonetic_intervals)

    print("Silence interval detected count: ", len(sqa.silence_intervals))
    print("Phonetic interval detected count: ", len(sqa.phonetic_intervals))

    print('Silence interval quality: ', si_q)
    print('Phonetic interval quality: ', pi_q)

    total_silence_time = np.sum(sqa.silence_intervals) / 1000
    total_phonetic_time = np.sum(sqa.phonetic_intervals) / 1000

    window_length_samples = (args.MOVING_WINDOW_PERCENTS/100) * samplerate
    window_count = int(audio_length_samples / window_length_samples)
    processed_length_seconds = (window_count * window_length_samples) / samplerate
    unprocessed_tail_seconds = (audio_file_duration_seconds - processed_length_seconds)

    print('Total silence percent: ', (total_silence_time/(processed_length_seconds)) * 100, "% /", total_silence_time, "seconds")
    print('Total phonetic percent: ', (total_phonetic_time/(processed_length_seconds)) * 100, "% /", total_phonetic_time, "seconds")
    print('Unprocessed tail length seconds: ', unprocessed_tail_seconds)
    print('Silent pause count:', len(sqa.silence_pause_intervals))

    if args.DEBUG_PLOT:
        plot(left, right, sqa, resample_to)
else:
    print("Invalid input file: " + audioFilePath)




#if __name__ == '__main__':


