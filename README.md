sqa_cli.py - command line util for speech quality analysis.
To get more info about arguments use ./sqa_cli.py -h command.

Example:

./sqa_cli.py ./data/1.wav --resample 2000 --to 60 --plot

Sample rate:  44100

File duration seconds:  347.98356009070295

Processing data...

Processed samples: 100 %

Processing done! Time seconds:  11.640063

Silence interval detected count:  76

Phonetic interval detected count:  77

Silence interval quality:  1.950657894736842

Phonetic interval quality:  3.5844155844155843

Total silence percent:  8.552616279069767 % / 29.421 seconds

Total phonetic percent:  8.801744186046513 % / 30.278 seconds

Unprocessed tail length seconds:  3.9835600907029516

Silent pause count: 30


![](images/intensity_graph_example.png)
