# Author: Voropay Andrey

import numpy
import math

class SQA:

    def __init__(self, sample_frequency,
                 resample_to=16000,
                 moving_window_width_percent=400,
                 callback=None,
                 lower_frequency_hz=200,
                 average_window_percent=2,
                 threshold_percent=3,
                 minimal_silence_interval_ms=10,
                 minimal_phonetic_interval_ms=10,
                 minimal_silence_pause_interval_ms=300):
        self.sample_frequency = sample_frequency
        self.resample_to = resample_to
        self.resample_ratio = sample_frequency/resample_to
        self.resampling_var = self.resample_ratio - 1
        self.average_window_samples = int((resample_to / 100.0) * average_window_percent)
        self.threshold_percent = threshold_percent/100
        self.average_window_array = numpy.zeros(self.average_window_samples)
        # Calculate window size for contour using the Shanon-Kotelnikov theorem
        self.moving_window_array_contour_samples = int((resample_to/lower_frequency_hz) * 2)
        self.moving_window_array_contour = numpy.zeros(self.moving_window_array_contour_samples)

        self.global_index = 0
        self.resampled_global_index = 0

        self.input_intensity_history = list()
        self.contouring_history = list()
        self.max_history = list()
        self.mean_history = list()
        self.threshold_history = list()

        self.silence_intervals = list()
        self.phonetic_intervals = list()
        self.silence_pause_intervals = list()

        self.silence_interval_history = list()
        self.phonetic_interval_history = list()

        self.total_max_amplitude = 0
        self.moving_window_mean = 0
        self.moving_window_max = 0

        self.average_window_samples = 0
        self.previous_total_intensity = 0
        self.start_of_last_interval_sample = 0
        self.end_of_last_interval_sample = 0
        self.moving_window_counter = 0
        self.interval_detected = False
        self.moving_window_samples = int((resample_to / 100.0) * moving_window_width_percent)
        self.moving_window_array = numpy.zeros(self.moving_window_samples)

        self.minimal_silence_interval_ms = minimal_silence_interval_ms
        self.minimal_phonetic_interval_ms = minimal_phonetic_interval_ms
        self.minimal_silence_pause_interval_ms = minimal_silence_pause_interval_ms

        self.old_intensity_threshold = 0
        if(callback != None):
            self.callback = callback

    def process_sample(self, sample_left, sample_right):
        new_intensity_threshold = 0
        self.resampling_var -= 1
        if self.resampling_var < 0:
            # Resampled data here
            abs_left = abs(sample_left)
            abs_right = abs(sample_right)
            # Mix two channels together
            total_intensity = (abs_left + abs_right) / 2
            self.input_intensity_history.append(total_intensity)

            # Fill a moving window
            self.moving_window_array = numpy.roll(self.moving_window_array, -1)
            self.moving_window_array[self.moving_window_samples - 1] = total_intensity

            # Fill contour moving max window
            self.moving_window_array_contour = numpy.roll(self.moving_window_array_contour, -1)
            self.moving_window_array_contour[self.moving_window_array_contour_samples - 1] = total_intensity
            # Get contour raw shape
            moving_window_array_contour_max = numpy.max(self.moving_window_array_contour)
            # Smooth the contour
            self.average_window_array = numpy.roll(self.average_window_array, -1)
            self.average_window_array[self.average_window_samples - 1] = moving_window_array_contour_max
            moving_window_contour_avg = numpy.average(self.average_window_array)

            self.contouring_history.append(moving_window_contour_avg)

            if self.moving_window_counter < (self.moving_window_samples - 1):
                self.moving_window_counter += 1
            else:
                # moving_window_array is filled and ready for processing
                self.moving_window_counter = 0
                if self.moving_window_mean == 0:
                    self.moving_window_mean = numpy.average(self.moving_window_array)
                else:
                    self.moving_window_mean = (self.moving_window_mean + numpy.average(self.moving_window_array)) / 2

                #if self.moving_window_max == 0:
                self.moving_window_max = numpy.max(self.moving_window_array)
                #else:
                #    self.moving_window_max = (self.moving_window_max + numpy.max(self.moving_window_array)) / 2

                if self.moving_window_max > self.total_max_amplitude:
                    self.total_max_amplitude = self.moving_window_max

                #new_intensity_threshold = ((self.moving_window_max - self.moving_window_mean) * self.threshold_percent) + self.moving_window_mean
                #new_intensity_threshold = new_intensity_threshold * 0.1 + self.old_intensity_threshold * 0.9
                intensity_threshold = ((self.moving_window_max - self.moving_window_mean) * self.threshold_percent) + self.moving_window_mean
                for i in range(self.moving_window_samples):
                    # Detection by the contour only
                    if self.contouring_history[self.resampled_global_index - (self.moving_window_samples - i)] >= intensity_threshold:
                        # Start of an interval
                        if self.interval_detected == False:
                            self.interval_detected = True
                            self.start_of_last_interval_sample = (self.resampled_global_index - (self.moving_window_samples - i))
                            if self.end_of_last_interval_sample != 0:
                                silence_interval_ms = ((self.start_of_last_interval_sample - self.end_of_last_interval_sample) / self.resample_to) * 1000
                                if silence_interval_ms >= self.minimal_silence_interval_ms:
                                    self.silence_intervals.append(silence_interval_ms)
                                    self.silence_interval_history.append([self.end_of_last_interval_sample, self.start_of_last_interval_sample])
                                if silence_interval_ms >= self.minimal_silence_pause_interval_ms:
                                    self.silence_pause_intervals.append(silence_interval_ms)


                    else:
                        # End of an interval
                        if self.interval_detected:
                            self.interval_detected = False
                            self.end_of_last_interval_sample = (self.resampled_global_index - (self.moving_window_samples - i))
                            if self.start_of_last_interval_sample != 0:
                                phonetic_interval_ms = ((self.end_of_last_interval_sample - self.start_of_last_interval_sample) / self.resample_to) * 1000
                                if phonetic_interval_ms >= self.minimal_phonetic_interval_ms:
                                    self.phonetic_intervals.append(phonetic_interval_ms)
                                    self.phonetic_interval_history.append([self.start_of_last_interval_sample, self.end_of_last_interval_sample])

                    self.mean_history.append(self.moving_window_mean)
                    self.max_history.append(self.moving_window_max)
                    self.threshold_history.append(intensity_threshold)
                # from_sample =
                # to_sample =

            # Increase resampling variables
            self.resampling_var += self.resample_ratio
            self.resampled_global_index += 1

            self.previous_total_intensity = total_intensity

        self.global_index += 1
        self.old_intensity_threshold = new_intensity_threshold


    def process_all(self, channel_left, channel_right):
        assert len(channel_left) == len(channel_right)
        for index in range(len(channel_left)):
            self.process_sample(channel_left[index], channel_right[index])